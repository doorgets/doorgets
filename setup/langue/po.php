<?php

$w['w00001'] = "Bem-vindo";
$w['w00002'] = "Termos de Serviço";
$w['w00003'] = "Verificar permissões de gravação";
$w['w00004'] = "Banco de dados de configuração";
$w['w00005'] = "Site Configuration";
$w['w00006'] = "Administrador do Sistema";
$w['w00007'] = "Copiar dados";
$w['w00008'] = "Instalação concluída";

$w['w00009'] = "Bem-vindo ao instalador doorGets CMS V4.1";
$w['w00010'] = "Termos de Serviço";
$w['w00011'] = "Verificar permissões de gravação";
$w['w00012'] = "Definir a conexão com o banco de dados";
$w['w00013'] = "Configure o seu site";
$w['w00014'] = "Defina o seu administrador";
$w['w00015'] = "Copiar dados para o seu espaço na web";
$w['w00016'] = "A instalação é concluída, Bravo";

$w['w00017'] = "Voltar";
$w['w00018'] = "Avançar";

$w['w00019'] = "Escolha seu idioma";
$w['w00020'] = "Eu aceito os termos de uso";

$w['w00021'] = "Teste o seu gravável";
$w['w00022'] = "O diretório é gravável.";
$w['w00023'] = "O diretório não tem permissões de gravação ...";

$w['w00024'] = "Ligue o seu banco de dados";
$w['w00025'] = "Host";
$w['w00026'] = "Nome do Banco";
$w['w00027'] = "Login";
$w['w00028'] = "Senha";
$w['w00029'] = "Prefixo";
$w['w00030'] = "Test Connection";

$w['w00031'] = "A conexão pode ser estabelecida se não atingir";
$w['w00032'] = "Sua conexão é estabelecida";

$w['w00033'] = "Website";
$w['w00034'] = "Título";
$w['w00035'] = "Slogan";
$w['w00036'] = "Descrição";
$w['w00037'] = "Copyright";
$w['w00038'] = "Ano da criação";
$w['w00039'] = "Palavras-chave";
$w['w00040'] = "Local";
$w['w00041'] = "País";
$w['w00042'] = "Cidade";
$w['w00043'] = "CEP";
$w['w00044'] = "Endereço";
$w['w00045'] = "Telefone Fixo";
$w['w00046'] = "Celular";
$w['w00047'] = "Fax";
$w['w00048'] = "Rede Social";
$w['w00049'] = "Estatísticas";
$w['w00050'] = "O Google Analytics código";
$w['w00051'] = "O Google Analytics é um serviço gratuito de análise de sites oferecidos pelo Google";
$w['w00052'] = "Crie sua conta de graça";
$w['w00053'] = "Sistema de identificadores para a sua aministration";
$w['w00054'] = "Endereço de email";
$w['w00055'] = "Login";
$w['w00056'] = "Senha";

$w['w00057'] = "Gerar seus FCMS doorGets Tutorial";
$w['w00058'] = "Gerar o meu site agora";

$w['w00059'] = "criados e oferecidos por";
$w['w00060'] = "Todos os direitos reservados";

$w['w00061'] = "Escolha uma cor para o seu site";