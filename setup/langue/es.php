<?php

$w['w00001'] = "Bienvenido";
$w['w00002'] = "Términos de Servicio";
$w['w00003'] = "Comprobar permisos de escritura";
$w['w00004'] = "Base de datos de configuración";
$w['w00005'] = "Sitio Web de configuración";
$w['w00006'] = "Administrador del sistema";
$w['w00007'] = "Copia de datos";
$w['w00008'] = "Instalación completa";

$w['w00009'] = "Bienvenido al instalador doorGets CMS V4.1";
$w['w00010'] = "Términos de Licencia de uso";
$w['w00011'] = "Comprobar permisos de escritura";
$w['w00012'] = "Configuración de la conexión a la base de datos";
$w['w00013'] = "Configure su sitio web";
$w['w00014'] = "Configurar su Administrador";
$w['w00015'] = "Copia de datos a su espacio web";
$w['w00016'] = "La instalación se ha completado, Bravo";

$w['w00017'] = "Volver";
$w['w00018'] = "Siguiente";

$w['w00019'] = "Cambiar la lengua";
$w['w00020'] = "Acepto los términos de uso";

$w['w00021'] = "Prueba de su escritura";
$w['w00022'] = "El directorio tiene permisos de escritura.";
$w['w00023'] = "El directorio no tiene permisos de escritura ...";

$w['w00024'] = "Conexión de la base de datos";
$w['w00025'] = "Host";
$w['w00026'] = "Nombre de la base de datos";
$w['w00027'] = "Inicio de sesión";
$w['w00028'] = "contraseña";
$w['w00029'] = "Prefijo";
$w['w00030'] = "Probar conexión";

$w['w00031'] = "La conexión se puede establecer que no llega a";
$w['w00032'] = "La conexión está establecida";

$w['w00033'] = "Sitio Web";
$w['w00034'] = "Título";
$w['w00035'] = "Slogan";
$w['w00036'] = "Descripción";
$w['w00037'] = "Derecho de autor";
$w['w00038'] = "Año de la creación";
$w['w00039'] = "Palabras clave";
$w['w00040'] = "Ubicación";
$w['w00041'] = "País";
$w['w00042'] = "Ciudad";
$w['w00043'] = "Código postal";
$w['w00044'] = "Dirección";
$w['w00045'] = "Teléfono fijo";
$w['w00046'] = "Teléfono Móvil";
$w['w00047'] = "Fax";
$w['w00048'] = "Red Social";
$w['w00049'] = "Estadísticas";
$w['w00050'] = "Su código de Google Analytics";
$w['w00051'] = "Google Analytics es un servicio gratuito de análisis de sitios web que ofrece Google";
$w['w00052'] = "Crea tu cuenta gratis";
$w['w00053'] = "Sistema de identificadores para su aministration";
$w['w00054'] = "Dirección de correo electrónico";
$w['w00055'] = "Inicio de sesión";
$w['w00056'] = "contraseña";

$w['w00057'] = "Genera tus FCMS doorGets Tutorial";
$w['w00058'] = "Crear mi sitio web ahora";

$w['w00059'] = "creado y ofrecido por";
$w['w00060'] = "Todos los derechos reservados";

$w['w00061'] = "Elija un color para su sitio web";