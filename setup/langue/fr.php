<?php

$w['w00001'] = "Bienvenue";
$w['w00002'] = "Licence & Conditions d'utilisation";
$w['w00003'] = "Vérification des droits d'écritures";
$w['w00004'] = "Configuration Base de données";
$w['w00005'] = "Configuration Site Web";
$w['w00006'] = "Configuration Administrateur";
$w['w00007'] = "Copie des données";
$w['w00008'] = "Installation terminée";

$w['w00009'] = "Bienvenue sur l'installateur doorGets CMS V4.1";
$w['w00010'] = "Licence & Conditions d'utilisation";
$w['w00011'] = "Vérification des droits d'écritures";
$w['w00012'] = "Configurer la connexion à la base de données";
$w['w00013'] = "Parametrer votre site web";
$w['w00014'] = "Configurer votre Administrateur";
$w['w00015'] = "Copie des données sur votre espace web";
$w['w00016'] = "L'instalation est terminée, Bravo";

$w['w00017'] = "Précédent";
$w['w00018'] = "Suivant";

$w['w00019'] = "Choisir votre langue";
$w['w00020'] = "J'accepte les conditions d'utilisations";

$w['w00021'] = "Test de vos droits d'écriture";
$w['w00022'] = "Votre dossier est bien en mode écriture.";
$w['w00023'] = "Votre dossier n'a pas les droits d'écriture...";

$w['w00024'] = "Connectez votre base de données";
$w['w00025'] = "Hôte";
$w['w00026'] = "Nom de la base";
$w['w00027'] = "Login";
$w['w00028'] = "Mot de passe";
$w['w00029'] = "Prefix";
$w['w00030'] = "Tester la connexion";

$w['w00031'] = "La connexion n'à pas pu etre établie";
$w['w00032'] = "Votre connexion est établie";

$w['w00033'] = "Site Web";
$w['w00034'] = "Titre";
$w['w00035'] = "Slogan";
$w['w00036'] = "Description";
$w['w00037'] = "Copyright";
$w['w00038'] = "Année de création";
$w['w00039'] = "Mots clés";
$w['w00040'] = "Localisation";
$w['w00041'] = "Pays";
$w['w00042'] = "Ville";
$w['w00043'] = "Code Postal";
$w['w00044'] = "Adresse";
$w['w00045'] = "Telephone fixe";
$w['w00046'] = "Telephone mobile";
$w['w00047'] = "Telephone fax";
$w['w00048'] = "Réseaux Sociaux";
$w['w00049'] = "Statistiques";
$w['w00050'] = "Votre code Google Analytics";
$w['w00051'] = "Google Analytics est un service d'analyse de sites web gratuit proposé par Google";
$w['w00052'] = "Créer votre compte gratuitement sur :";
$w['w00053'] = "Identifiants pour votre systeme d'aministration";
$w['w00054'] = "Adresse email";
$w['w00055'] = "Login";
$w['w00056'] = "Mot de passe";

$w['w00057'] = "Générer votre FCMS doorGets Tutoriel";
$w['w00058'] = "Générer mon site maintenant";

$w['w00059'] = "Créer et offert par";
$w['w00060'] = "Tous droits réservés";

$w['w00061'] = "Choisir une couleur pour votre site";

