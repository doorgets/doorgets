<?php

$w['w00001'] = "Welcome";
$w['w00002'] = "Terms of Service";
$w['w00003'] = "Checking write permissions";
$w['w00004'] = "Configuration Database";
$w['w00005'] = "Configuration Website";
$w['w00006'] = "System Administrator";
$w['w00007'] = "Copying data";
$w['w00008'] = "Installation complete";

$w['w00009'] = "Welcome to the installer doorGets CMS V4.1";
$w['w00010'] = "Licensing Terms of Service";
$w['w00011'] = "Checking write permissions";
$w['w00012'] = "Set the connection to the database";
$w['w00013'] = "Set up your website";
$w['w00014'] = "Set your Administrator";
$w['w00015'] = "Copying data to your web space";
$w['w00016'] = "The instalation is completed, Bravo";

$w['w00017'] = "Back";
$w['w00018'] = "Next";

$w['w00019'] = "Choose your language";
$w['w00020'] = "I accept the terms of use";

$w['w00021'] = "Test your writable";
$w['w00022'] = "Your directory is writable.";
$w['w00023'] = "Your directory does not have write permissions ...";

$w['w00024'] = "Connect your database";
$w['w00025'] = "Host";
$w['w00026'] = "Database Name";
$w['w00027'] = "Login";
$w['w00028'] = "Password";
$w['w00029'] = "Prefix";
$w['w00030'] = "Test Connection";

$w['w00031'] = "The connection could be established it does not reach";
$w['w00032'] = "Your connection is established";

$w['w00033'] = "Website";
$w['w00034'] = "Title";
$w['w00035'] = "Slogan";
$w['w00036'] = "Description";
$w['w00037'] = "Copyright";
$w['w00038'] = "Year of creation";
$w['w00039'] = "Keywords";
$w['w00040'] = "Location";
$w['w00041'] = "Country";
$w['w00042'] = "City";
$w['w00043'] = "Zip Code";
$w['w00044'] = "Address";
$w['w00045'] = "Fixed Telephone";
$w['w00046'] = "Mobile Phone";
$w['w00047'] = "Telephone Fax";
$w['w00048'] = "Social Network";
$w['w00049'] = "Statistics";
$w['w00050'] = "Your Google Analytics code";
$w['w00051'] = "Google Analytics is an analytics service free websites offered by Google";
$w['w00052'] = "Create your account for free ";
$w['w00053'] = "System of identifiers for your aministration";
$w['w00054'] = "Email Address";
$w['w00055'] = "Login";
$w['w00056'] = "Password";

$w['w00057'] = "Generate your FCMS doorGets Tutorial";
$w['w00058'] = "Generate my website now";

$w['w00059'] = "Created and Offered by";
$w['w00060'] = "All rights reserved";

$w['w00061'] = "Choose a color for your website";