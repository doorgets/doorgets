<?php

$w['w00001'] = "Benvenuto";
$w['w00002'] = "Termini di servizio";
$w['w00003'] = "Verifica i permessi di scrittura";
$w['w00004'] = "Banca dati di configurazione";
$w['w00005'] = "Sito web di configurazione";
$w['w00006'] = "Amministratore di sistema";
$w['w00007'] = "Copia di dati";
$w['w00008'] = "Installazione completata";

$w['w00009'] = "Benvenuti al programma di installazione doorGets V4.0";
$w['w00010'] = "Termini della licenza di servizio";
$w['w00011'] = "Verifica permessi di scrittura";
$w['w00012'] = "Imposta la connessione al database";
$w['w00013'] = "Imposta il tuo sito web";
$w['w00014'] = "Imposta l'amministratore";
$w['w00015'] = "Copia di dati sul tuo spazio web";
$w['w00016'] = "Il instalation è completato, Bravo";

$w['w00017'] = "Indietro";
$w['w00018'] = "Avanti";

$w['w00019'] = "Scegli la tua lingua";
$w['w00020'] = "Accetto i termini di utilizzo";

$w['w00021'] = "Test tuo scrivibile";
$w['w00022'] = "La directory è scrivibile.";
$w['w00023'] = "La directory non dispone di autorizzazioni di scrittura ...";

$w['w00024'] = "Collegare il database";
$w['w00025'] = "Host";
$w['w00026'] = "Nome database";
$w['w00027'] = "Login";
$w['w00028'] = "Password";
$w['w00029'] = "Prefisso";
$w['w00030'] = "Test di connessione";

$w['w00031'] = "La connessione può essere stabilita non arriva";
$w['w00032'] = "La connessione è stabilita";

$w['w00033'] = "Sito Web";
$w['w00034'] = "Titolo";
$w['w00035'] = "Slogan";
$w['w00036'] = "Descrizione";
$w['w00037'] = "Copyright";
$w['w00038'] = "Anno della creazione";
$w['w00039'] = "Parole chiave";
$w['w00040'] = "Posizione";
$w['w00041'] = "Nazione";
$w['w00042'] = "Città";
$w['w00043'] = "CAP";
$w['w00044'] = "Indirizzo";
$w['w00045'] = "Telefono fisso";
$w['w00046'] = "Cellulare";
$w['w00047'] = "Telefono Fax";
$w['w00048'] = "Social Network";
$w['w00049'] = "Statistiche";
$w['w00050'] = "Il tuo codice di Google Analytics";
$w['w00051'] = "Google Analytics è un servizio di analisi web gratuiti offerti da Google";
$w['w00052'] = "Crea il tuo account gratis";
$w['w00053'] = "Sistema di identificatori per il tuo aministration";
$w['w00054'] = "Indirizzo email";
$w['w00055'] = "Login";
$w['w00056'] = "Password";

$w['w00057'] = "Crea il tuo FCMS doorGets Tutorial";
$w['w00058'] = "Crea il mio sito ora";

$w['w00059'] = "Creato e Offerto da";
$w['w00060'] = "Tutti i diritti riservati";

$w['w00061'] = "Scegliere un colore per il tuo sito";