<?php

$w['w00001'] = "Välkommen";
$w['w00002'] = "Användarvillkor";
$w['w00003'] = "Kontrollera skrivrättigheter";
$w['w00004'] = "Konfigurationsdatabas";
$w['w00005'] = "Konfiguration webbplats";
$w['w00006'] = "Systemadministratör";
$w['w00007'] = "Kopiera data";
$w['w00008'] = "Installation klar";

$w['w00009'] = "Välkommen till installationsprogrammet doorGets CMS V4.1";
$w['w00010'] = "Licensvillkor för Service";
$w['w00011'] = "Kontrollera skrivrättigheter";
$w['w00012'] = "Ange anslutningen till databasen";
$w['w00013'] = "Ställ in din webbplats";
$w['w00014'] = "Ställ in din administratör";
$w['w00015'] = "Kopiera data till ditt webbutrymme";
$w['w00016'] = "Det instalation är klar Bravo";

$w['w00017'] = "Tillbaka";
$w['w00018'] = "Nästa";

$w['w00019'] = "Välj språk";
$w['w00020'] = "Jag accepterar användarvillkoren";

$w['w00021'] = "Testa din skrivbar";
$w['w00022'] = "Din katalog är skrivbar.";
$w['w00023'] = "Din katalogen inte har skrivbehörighet ...";

$w['w00024'] = "Anslut din databas";
$w['w00025'] = "värd";
$w['w00026'] = "Databas Namn";
$w['w00027'] = "Logga in";
$w['w00028'] = "Lösenord";
$w['w00029'] = "Prefix";
$w['w00030'] = "Testa anslutning";

$w['w00031'] = "Anslutningen kunde fastställas att det inte når";
$w['w00032'] = "Din anslutning upprättas";

$w['w00033'] = "Webbplatsen";
$w['w00034'] = "Titel";
$w['w00035'] = "Slogan";
$w['w00036'] = "Beskrivning";
$w['w00037'] = "Upphovsrätt";
$w['w00038'] = "År av skapelsen";
$w['w00039'] = "Nyckelord";
$w['w00040'] = "Plats";
$w['w00041'] = "Land";
$w['w00042'] = "City";
$w['w00043'] = "Postnummer";
$w['w00044'] = "Adress";
$w['w00045'] = "Fast telefon";
$w['w00046'] = "Mobiltelefon";
$w['w00047'] = "Telefon Fax";
$w['w00048'] = "Social Network";
$w['w00049'] = "Statistik";
$w['w00050'] = "Din Google Analytics kod";
$w['w00051'] = "Google Analytics är ett Analytics tjänsten gratis webbplatser som Google erbjuder";
$w['w00052'] = "Skapa ditt konto gratis";
$w['w00053'] = "System av identifierare för din aministration";
$w['w00054'] = "E-postadress";
$w['w00055'] = "Logga in";
$w['w00056'] = "Lösenord";

$w['w00057'] = "Skapa dina FCMS doorGets Handledning";
$w['w00058'] = "Skapa min hemsida nu";

$w['w00059'] = "Skapad och erbjuds av";
$w['w00060'] = "All rights reserved";

$w['w00061'] = "Välj en färg för din webbplats";