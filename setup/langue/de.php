<?php

$w['w00001'] = "Willkommen";
$w['w00002'] = "Terms of Service";
$w['w00003'] = "Überprüfen Schreibrechte";
$w['w00004'] = "Configuration Database";
$w['w00005'] = "Configuration Website";
$w['w00006'] = "System Administrator";
$w['w00007'] = "Kopieren von Daten";
$w['w00008'] = "Installation abgeschlossen";

$w['w00009'] = "Willkommen auf den Installateur doorGets CMS V4.1";
$w['w00010'] = "Licensing Terms of Service";
$w['w00011'] = "Überprüfen Schreibrechte";
$w['w00012'] = "Stellen Sie die Verbindung zur Datenbank";
$w['w00013'] = "Richten Sie Ihre Website";
$w['w00014'] = "Stellen Sie Ihren Administrator";
$w['w00015'] = "Kopieren von Daten auf Ihren Webspace";
$w['w00016'] = "Die instalation abgeschlossen ist, Bravo";

$w['w00017'] = "Zurück";
$w['w00018'] = "Weiter";

$w['w00019'] = "Wählen Sie Ihre Sprache";
$w['w00020'] = "Ich akzeptiere die Bedingungen der Nutzung";

$w['w00021'] = "Test your schreibbar";
$w['w00022'] = "Ihr Verzeichnis beschreibbar ist.";
$w['w00023'] = "Ihr Verzeichnis keine Schreibrechte ...";

$w['w00024'] = "Schließen Sie Ihre Datenbank";
$w['w00025'] = "Host";
$w['w00026'] = "Database Name";
$w['w00027'] = "Login";
$w['w00028'] = "Passwort";
$w['w00029'] = "Prefix";
$w['w00030'] = "Test Connection";

$w['w00031'] = "Die Verbindung konnte festgestellt werden es nicht erreichen werden";
$w['w00032'] = "Ihre Verbindung wird hergestellt";

$w['w00033'] = "Website";
$w['w00034'] = "Titel";
$w['w00035'] = "Slogan";
$w['w00036'] = "Beschreibung";
$w['w00037'] = "Copyright";
$w['w00038'] = "Jahr der Schöpfung";
$w['w00039'] = "Keywords";
$w['w00040'] = "Ort";
$w['w00041'] = "Land";
$w['w00042'] = "City";
$w['w00043'] = "Zip Code";
$w['w00044'] = "Adresse";
$w['w00045'] = "Fixed Telephone";
$w['w00046'] = "Mobile Phone";
$w['w00047'] = "Telefon Fax";
$w['w00048'] = "Social Network";
$w['w00049'] = "Statistik";
$w['w00050'] = "Ihre Google Analytics-Code";
$w['w00051'] = "Google Analytics ist ein Google Analytics-Service kostenlos Webseiten von Google angebotenen";
$w['w00052'] = "Erstellen Sie Ihr Konto kostenlos";
$w['w00053'] = "System von Identifikatoren für Ihre aministration";
$w['w00054'] = "E-Mail-Adresse";
$w['w00055'] = "Login";
$w['w00056'] = "Passwort";

$w['w00057'] = "Erstellen Sie Ihre FCMS doorGets Tutorial";
$w['w00058'] = "Generieren meine Website jetzt";

$w['w00059'] = "Erstellt und Angeboten von";
$w['w00060'] = "Alle Rechte vorbehalten";

$w['w00061'] = "Wählen Sie eine Farbe für Ihre Website";