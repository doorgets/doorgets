<?php

$w['w00001'] = "Witaj";
$w['w00002'] = "Warunki korzystania z usługi";
$w['w00003'] = "Sprawdzanie zapisu";
$w['w00004'] = "Konfiguracja bazy danych";
$w['w00005'] = "Konfiguracja strony";
$w['w00006'] = "Administrator systemu";
$w['w00007'] = "Kopiowanie danych";
$w['w00008'] = "Instalacja zakończona";

$w['w00009'] = "Witamy w instalatorze doorGets CMS V4.1";
$w['w00010'] = "Warunki licencyjne z usługi";
$w['w00011'] = "Sprawdzanie uprawnienia do zapisu";
$w['w00012'] = "Ustawianie połączenia z bazą danych";
$w['w00013'] = "Skonfiguruj swoją stronę";
$w['w00014'] = "Ustaw Administratora";
$w['w00015'] = "Kopiowanie danych do przestrzeni internetowej";
$w['w00016'] = "instalacyjne zakończeniu Bravo";

$w['w00017'] = "Powrót";
$w['w00018'] = "Dalej";

$w['w00019'] = "Wybierz język";
$w['w00020'] = "Akceptuję warunki użytkowania";

$w['w00021'] = "Testuj zapisywalny";
$w['w00022'] = "Twój katalog jest zapisywalny.";
$w['w00023'] = "Katalog nie ma uprawnień zapisu ...";

$w['w00024'] = "Połącz z bazy danych";
$w['w00025'] = "Host";
$w['w00026'] = "Nazwa bazy danych";
$w['w00027'] = "Login";
$w['w00028'] = "Hasło";
$w['w00029'] = "Prefiks";
$w['w00030'] = "Test połączenia";

$w['w00031'] = "Połączenie może być ustalone, nie osiąga";
$w['w00032'] = "Twoje połączenie zostanie ustanowione";

$w['w00033'] = "Strona WWW";
$w['w00034'] = "Nazwa";
$w['w00035'] = "Hasło";
$w['w00036'] = "Opis";
$w['w00037'] = "Prawa autorskie";
$w['w00038'] = "Rok powstania";
$w['w00039'] = "Słowa kluczowe";
$w['w00040'] = "Location";
$w['w00041'] = "Kraj";
$w['w00042'] = "Miasto";
$w['w00043'] = "Kod pocztowy";
$w['w00044'] = "Adres";
$w['w00045'] = "stacjonarny";
$w['w00046'] = "Telefon komórkowy";
$w['w00047'] = "Telefon Faks";
$w['w00048'] = "Social Network";
$w['w00049'] = "Statystyka";
$w['w00050'] = "Twój kod Google Analytics";
$w['w00051'] = "Google Analytics to usługa analityczna darmowych witryn oferowanych przez Google";
$w['w00052'] = "Utwórz konto za darmo";
$w['w00053'] = "System identyfikatorów dla Twojego aministration";
$w['w00054'] = "Adres e-mail";
$w['w00055'] = "Login";
$w['w00056'] = "Hasło";

$w['w00057'] = "wygenerować FCMS doorGets Samouczek";
$w['w00058'] = "Generowanie moją stronę teraz";

$w['w00059'] = "Stworzony i oferowana przez";
$w['w00060'] = "Wszystkie prawa zastrzeżone";

$w['w00061'] = "Wybierz kolor, na swojej stronie internetowej";